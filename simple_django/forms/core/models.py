from django.db import models
from django.db.models.signals import pre_save
from core.utils import notify


class Logger(models.Model):
    time_created = models.DateTimeField(auto_now=False, auto_now_add=True)
    request_path = models.CharField(max_length=255, null=True)
    request_method = models.CharField(max_length=255, null=True)
    execution_time = models.FloatField(null=True)

    class Meta:
        verbose_name = "Log"
        verbose_name_plural = "Logs"

    def __str__(self):
        return self.request_path


class Student(models.Model):
    phone = models.CharField(max_length=255, verbose_name="Телефон", null=True)
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    last_name = models.CharField(max_length=255, verbose_name="Фамилия", null=True)
    age = models.IntegerField(null=True)

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"

    def clean(self):
        self.first_name = self.first_name.capitalize()
        self.last_name = self.last_name.capitalize()

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class Teacher(models.Model):
    first_name = models.CharField(max_length=255, verbose_name="Имя")
    last_name = models.CharField(max_length=255, verbose_name="фамилия", null=True)

    class Meta:
        verbose_name = "Учитель"
        verbose_name_plural = "Учителя"

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Group(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название группы")
    students = models.ManyToManyField("core.Student", blank=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        return self.name


def send_notify(instance, **kwargs):
    notify(instance)


pre_save.connect(send_notify, sender=Student)
pre_save.connect(send_notify, sender=Teacher)
