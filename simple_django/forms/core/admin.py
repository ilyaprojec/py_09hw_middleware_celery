from django.contrib import admin
from core.models import Student, Group, Teacher, Logger


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'first_name', 'last_name', 'age', 'phone', )
    search_fields = ('last_name', )
    list_filter = ('group', )


class LoggerAdmin(admin.ModelAdmin):
    list_display = ('time_created', 'request_path', 'request_method', 'execution_time')


admin.site.register(Group, )
admin.site.register(Student, StudentAdmin, )
admin.site.register(Teacher, )
admin.site.register(Logger, LoggerAdmin)
