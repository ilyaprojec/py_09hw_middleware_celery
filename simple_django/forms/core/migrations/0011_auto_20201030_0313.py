from django.db import migrations


def combine_names(apps, schema_editor):
    Student = apps.get_model('core', 'Student')
    Teacher = apps.get_model('core', 'Teacher')

    for student in Student.objects.all():
        student.name = '%s %s' % (student.first_name, student.last_name)
        student.save()

    for teacher in Teacher.objects.all():
        teacher.name = '%s %s' % (teacher.first_name, teacher.last_name)
        teacher.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20201030_0103'),
    ]

    operations = [
        migrations.RunPython(combine_names),
    ]
